/*jshint esnext: true */
'use strict';

const List = function (iterable) {

    this.toArray = () => {
        const arr = [];
        this.forEach(el => arr.push(el));
        return arr;
    };

    this.take = n => new List((function*() {
        for (const el of iterable) {
            if (n-- === 0) {
                break;
            }
            yield el;
        }
    })());

    this.forEach = f => {
        for (const el of iterable) {
            f(el);
        }
    };

    this.head = head();

    function head() {
        //noinspection LoopStatementThatDoesntLoopJS
        for (const el of iterable) {
            return el;
        }
    }

    this[Symbol.iterator] = function* () {
        for (const el of iterable) {
            yield el;
        }
    };

};

module.exports = List;
