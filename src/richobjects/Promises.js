/*jshint esnext: true */
'use strict';

Promise.prototype.onComplete = function(f) {
    return this.then(f, f);
};

Promise.prototype.logOnFailure = function () {
    return this.catch(error => console.error(error));
};

Promise.prototype.flatMap = function (f) {
    const fun = typeof f === 'function' ? f : () => f;

    return new Promise((resolve, reject) => {
            this.then(
                success1 => {
                    try {
                        fun(success1).then(
                            success2 => resolve(success2),
                            error2 => reject(error2)
                        );
                    } catch (e) {
                        reject(e);
                    }
                },
                error1 => reject(error1)
            );
    });
};
