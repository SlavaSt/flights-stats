'use strict';

// [B](f: (A) ⇒ [B]): [B]  ; Although the types in the arrays aren't strict (:
Array.prototype.flatMap = function(f) {
    return Array.prototype.concat.apply([], this.map(f));
};
