/*jshint esnext: true */
'use strict';

const log = msg => (msg => console.log(msg));

module.exports = log;
