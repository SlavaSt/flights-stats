/*jshint esnext: true */
'use strict';

require('../richobjects/Promises');
require('../richobjects/Dates');
const List = require('../richobjects/List');
const Promise = require("bluebird"); // jshint ignore:line
const co = Promise.coroutine;
const equal = require('deep-equal');

const wizzair = require('../feeds/wizzair/wizzair');
const wizzairRepo = require('../service/wizzairRepo.js');
require('../richobjects/Promises');

const wizzairService = {};

const updatePrice = flight => co(function* () {
    const repo = yield wizzairRepo;
    const retrieved = yield repo.findFlight(flight);

    if (!retrieved) {
        updatePricesHistory(flight);
        return yield repo.saveFlight(flight);
    }

    if (equal(retrieved.minPrice, flight.minPrice)) {
        return yield repo.updateTimestamp(flight);
    }

    retrieved.minPrice = flight.minPrice;
    updatePricesHistory(retrieved);
    return yield repo.updateFlight(retrieved);
})();
wizzairService.updatePrice = updatePrice;

function updatePricesHistory(flight) {
    if (!flight.prices) {
        flight.prices = [];
    }
    flight.prices.push({
        date: new Date(),
        currency: flight.minPrice.currency,
        value: flight.minPrice.value
    });
}

wizzairService.collectFlightStats = co(function* () {
    const routes = yield wizzair.getRoutes();

    const routesList = new List(routes);
    const resultingPromises = [];
    for (const route of routesList) {
        const resultingPromise = wizzair.getPrices(route.from, route.to, new Date().addDays(1), new Date().addDays(365)).onResult(prices => {
            prices.forEach(updatePrice);
        });
        resultingPromises.push(resultingPromise);
    }
    yield Promise.all(resultingPromises);
});

wizzairService.close = co(function* () {
    const repo = yield wizzairRepo;
    repo.close();
});


module.exports = wizzairService;
