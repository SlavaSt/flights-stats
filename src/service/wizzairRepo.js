/*jshint esnext: true */
'use strict';

require('../richobjects/Promises');
const Promise = require("bluebird"); // jshint ignore:line
const co = Promise.coroutine;
const extend = require('util')._extend;
const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017/flightStats';

const repo = {};

module.exports = co(function* () {
    const db = yield MongoClient.connect(url).logOnFailure();
    console.log(`Connected to db: ${url}`);
    const flights = db.collection('flights');
    const indexesResult = yield createIndexes(flights);
    console.log("created indexes: " + indexesResult);
    const archive = db.collection('flightsArchive');


    repo.updateTimestamp = flight => {
        return flights.updateOne(template(flight), {
            $set: {
                updatedTimestamp: new Date()
            }
        });
    };

    repo.dropFlights = () => {
        return flights.deleteMany({});
    };

    repo.updateFlight = flight => {
        return flights.updateOne(template(flight), updateTimestamp(flight));
    };

    repo.saveFlight = flight => {
        return flights.insertOne(appendTimestamp(flight));
    };

    repo.archiveFlight = flight => {
        return archive.insertOne(updateTimestamp(flight));
    };

    repo.findFlight = flight => {
        return flights.find(template(flight)).limit(1).next();
    };

    repo.close = () => db.close();

    return repo;
})();

function template(flight) {
    return {
        from: flight.from,
        to: flight.to,
        company: flight.company,
        departureDate: flight.departureDate,
        departureTime: flight.departureTime,
        arrivalTime: flight.arrivalTime
    };
}

function updateTimestamp(obj) {
    return extend({
        updatedTimestamp: new Date()
    }, obj);
}

function appendTimestamp(obj) {
    const now = new Date();
    return extend({
        createdTimestamp: now,
        updatedTimestamp: now
    }, obj);
}

function createIndexes(flights) {
    return flights.createIndex(
        {'from': 1, 'to': 1, 'company': 1, 'departureDate': 1, 'departureTime': 1, 'arrivalTime': 1},
        {unique: true}
    );
}
