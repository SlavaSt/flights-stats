/*jshint esnext: true */
'use strict';

const request = require('request');
const querystring = require('querystring');

const rest = {};

rest.get = (url, parameters) => {
    const query = `${url}?${querystring.stringify(parameters)}`;
    return new Promise((resolve, reject) => {
        request(query, (error, response, body) => {
            console.log(`query: ${query}`);
            if (response) {
                console.log(`Got response: ${response.statusCode}`);
            }
            if (error || response.statusCode !== 200) {
                reject(error || response);
            } else {
                resolve({response, body});
            }
        });
    });
};

rest.getBody = (url, parameters) => {
    return rest.get(url, parameters)
        .then(responseBody => responseBody.body);
};

rest.getJson = (url, parameters) => {
    return rest.getBody(url, parameters)
        .then(body => JSON.parse(body));
};

module.exports = rest;
