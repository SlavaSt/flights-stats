/*jshint esnext: true */
(() => {
    'use strict';

    require('../../richobjects/Promises');
    require('../../richobjects/Arrays');

    const rest = require('../../rest/rest');

    const routesUrl = 'https://cdn.static.wizzair.com/en-GB/Markets.js';
    const pricesUrl = 'https://cdn.static.wizzair.com/en-GB/TimeTableAjax'; // ?departureIATA=KTW&arrivalIATA=BVA&year=2015&month=12';

    const wizzairRest = {};

    //noinspection JSUnresolvedFunction
    wizzairRest.getRoutes = () => rest.getBody(routesUrl)
        .then(body => {
            const routes = getRoutes(body);
            return getFromTo(routes);
        });

    /*
     {"ArrivalStationCode":"BVA","CurrentDate":"01\/12\/2015","Date":"20151201","DepartureStationCode":"KTW","Flights":[{"CarrierCode":"W6","FlightNumber":"1051","STD":"11:35","STA":"13:45","ArrivalStationName":"Paris Beauvais","DepartureStationName":"Katowice","IsMACStation":"True","IsAirportChange":"False"}],"HasSelection":"True","InMonth":"True","MinimumPrice":"zł59.00"},
     */
    wizzairRest.getPrices = (from, to, year, month) => {
        return rest.getJson(pricesUrl, {
                'departureIATA': from,
                'arrivalIATA': to,
                'year': year,
                'month': month
            })
            .then(flights => {
                return flights.flatMap(route => route.Flights.map(flight => {
                    return {
                        from, to,
                        company: 'WizzAir',
                        departureDate: toDate(route.CurrentDate),
                        departureDateString: route.CurrentDate,
                        departureTime: flight.STD,
                        arrivalTime: flight.STA,
                        minPrice: parsePrice(route.MinimumPrice)
                    };
                }));
            })
            .catch(error => {
                console.log(`retrying: ${from} ${to} ${year} ${month}` );
                return wizzairRest.getPrices(from, to, year, month);
            });
    };

    module.exports = wizzairRest;

    /**Creates Date object from string in format dd/mm/yyyy  */
    function toDate(dateString){
        var split = dateString.split("/");
        return new Date(Date.UTC(split[2], split[1]-1, split[0]));
    }

    function getFromTo(routes) {
        return routes.flatMap(route => {
            //noinspection JSUnresolvedVariable
            const to = route.DS;
            //noinspection JSUnresolvedVariable
            const fromList = route.ASL;
            //noinspection JSUnresolvedVariable
            return fromList.map(from => from.SC).map(from => {
                return {from, to};
            });
        });
    }

    function parsePrice(priceString) {
        const price = /(\D+)([\d,\.]+)/.exec(priceString);

        const priceAsNum = parseFloat(price[2].replace(",", ""));
        if (!price) {
            throw `cannot parse price: ${priceString}`;
        }

        return {
            currency: price[1],
            value: priceAsNum
        };
    }

})();

function getRoutes(body) {
    eval(body); // jshint ignore:line
    //noinspection JSUnresolvedVariable
    return JSON.parse(wizzAutocomplete.MARKETINFO); // jshint ignore:line
}




