/*jshint esnext: true */
'use strict';

const wizzairRest = require('./wizzairRest');

var wizzair = {};

/**
 * returns Promise
 */
wizzair.getRoutes = () => {
    return wizzairRest.getRoutes().logOnFailure();
};

wizzair.getPrices = (from, to, fromDate, toDate, callback) => {
    const yearFrom = fromDate.getFullYear();
    const monthFrom = fromDate.getMonth() + 1;
    const yearTo = toDate.getFullYear();
    const monthTo = toDate.getMonth() + 1;
    callback = typeof callback === 'function' ? callback : () => {
    };

    const iter = (function*() {
        for (let year = yearFrom; year <= yearTo; year++) {
            const minMonth = year === yearFrom ? monthFrom : 1;
            const maxMonth = year === yearTo ? monthTo : 12;
            for (let month = minMonth; month <= maxMonth; month++) {
                yield {year, month};
            }
        }
    })();

    const prices = [];

    function getPricesRecursive(date) {
        if (date.done) {
            return Promise.resolve(prices);
        }
        return wizzairRest.getPrices(from, to, date.value.year, date.value.month).flatMap(result => {
            if (result.length === 0) {
                return Promise.resolve([]);
            }
            callback(result);
            prices.push(...result);
            return getPricesRecursive(iter.next());
        });
    }

    const result = getPricesRecursive(iter.next()).logOnFailure();
    result.onResult = f => {
        callback = f;
        return result;
    };
    return result;
};

module.exports = wizzair;
