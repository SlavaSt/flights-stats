/*jshint esnext: true */
/*jshint mocha: true */
'use strict';

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const wizzairRepo = require('../src/service/wizzairRepo.js');
const Promise = require('bluebird'); // jshint ignore:line
const co = Promise.coroutine;


describe('wizzairRepo', () => {
    describe('.saveFlight()', () => {
        it('should drop flight collection first', (done) => {
            co(function* () {
                const repo = yield wizzairRepo;
                const result = repo.dropFlights();
                console.log("dropping collection");
                expect(result).to.be.fulfilled.notify(done);
            })();

        });

        it('should save flight to db', (done) => {
            const flight = {
                from: "KBP",
                to: "LDN",
                company: 'WizzAir',
                departureDate: "01/01/2001",
                departureTime: "10:30",
                arrivalTime: "11:30",
                prices: [
                    {
                        date: new Date(),
                        currency: "$",
                        value: "120"
                    }
                ],
                minPrice: {
                    currency: "$",
                    value: "120"
                }
            };
            co(function* () {
                const repo = yield wizzairRepo;
                const result = repo.saveFlight(flight);
                console.log("saving first flight");
                expect(result).to.be.fulfilled.notify(done);
                return yield result;
            })();
        });

        it("should find flight", done => {
            const template = {
                from: "KBP",
                to: "LDN",
                company: 'WizzAir',
                departureDate: "01/01/2001",
                departureTime: "10:30",
                arrivalTime: "11:30"
            };

            co(function* () {
                const repo = yield wizzairRepo;
                const retrieved = repo.findFlight(template);
                expect(retrieved).to.eventually.have.property('from').notify(done);
                return yield retrieved;
            })();
        });

        it('should populate price history array for the first flight save', done => {
            const template = {
                from: "KBP",
                to: "LDN",
                company: 'WizzAir',
                departureDate: "01/01/2001",
                departureTime: "10:30",
                arrivalTime: "11:30"
            };

            co(function* () {
                const repo = yield wizzairRepo;
                const prices = repo.findFlight(template).then(flight => {
                    expect(flight.prices).to.have.length(1);
                    expect(flight.prices[0].value).to.equal('120');
                    expect(flight.prices[0].currency).to.equal('$');
                });
                expect(prices).to.be.fulfilled.notify(done);
                return yield prices;
            })();
        });


        it("should not allow two identical flights", done => {
            const flight = {
                from: "KBP",
                to: "LDN",
                company: 'WizzAir',
                departureDate: "01/01/2001",
                departureTime: "10:30",
                arrivalTime: "11:30",
                minPrice: {
                    currency: "$",
                    value: "120"
                }
            };
            const result = co(function* () {
                const repo = yield wizzairRepo;
                const result = repo.saveFlight(flight);
                console.log("saving second flight");
                return yield result;
            })();
            expect(result).to.be.rejectedWith("E11000 duplicate key error index").notify(done);
        });

        it("should not find non-existing flight", done => {
            const template = {
                from: "XXX",
                to: "LDN",
                company: 'WizzAir',
                departureDate: "01/01/2001",
                departureTime: "10:30",
                arrivalTime: "11:30"
            };
            co(function* () {
                const repo = yield wizzairRepo;
                const retrieved = repo.findFlight(template);
                expect(retrieved).to.eventually.be.null.notify(done);
            })();
        });

        it("should update updatedTimestamp", done => {
            const template = {
                from: "KBP",
                to: "LDN",
                company: 'WizzAir',
                departureDate: "01/01/2001",
                departureTime: "10:30",
                arrivalTime: "11:30"
            };
            co(function* () {
                const repo = yield wizzairRepo;
                const retrieved = yield repo.findFlight(template);

                yield repo.updateTimestamp(template);
                const updated = yield repo.findFlight(template);

                expect(retrieved.updatedTimestamp).to.be.below(updated.updatedTimestamp);
                done();
            })();
        });
    });
});
