/*jshint esnext: true */
/*jshint mocha: true */
'use strict';

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const Promise = require("bluebird");
const List = require('../src/richobjects/List');


describe("Lists", () => {
    it("should be iterable", () => {
        const list = [1, 2, 3];
        const actual = [];
        for (const el of new List(list)) {
            actual.push(el);
        }

        expect(actual).to.eql(list);
    });

    it("should return head of the list", () => {
        const list = [1, 2, 3];

        expect(1).to.eql(new List(list).head);
    });

    it("should convert list to array", () => {
        const list = [1, 2, 3];

        expect(list).to.eql(new List(list).toArray());
    });

    it("should take first 2 elements", () => {
        const list = [1, 2, 3];

        expect([1, 2]).to.eql(new List(list).take(3).toArray());
    });

    it("should demonstrate generators", () => {
        const naturalNumbers = function*() {
          var i = 1;
            //noinspection InfiniteLoopJS
            while(true) {
                yield i++;
            }
        };

        console.log(new List(naturalNumbers()).take(10).toArray());

        var i = 1;
        for (const num of naturalNumbers()) {
            console.log(num);
            if (i++ > 10) {
                break;
            }
        }
    });

});
