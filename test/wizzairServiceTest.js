/*jshint esnext: true */
/*jshint mocha: true */
'use strict';

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const wizzairRepo = require('../src/service/wizzairRepo.js');
const Promise = require('bluebird'); // jshint ignore:line
const co = Promise.coroutine;
require('../src/richobjects/Promises');

const wizzairService = require('../src/service/wizzairService.js');


describe('wizzairService', () => {
    describe('.updatePrice()', () => {
        it('should drop flight collection first', (done) => {
            co(function* () {
                const repo = yield wizzairRepo;
                const result = repo.dropFlights();
                console.log("dropping collection");
                expect(result).to.be.fulfilled.notify(done);
            })();

        });

        it('should init price history for new flight', (done) => {
            const flight = {
                from: "KBP",
                to: "LDN",
                company: 'WizzAir',
                departureDate: "01/01/2001",
                departureTime: "10:30",
                arrivalTime: "11:30",
                minPrice: {
                    currency: "$",
                    value: "120"
                }
            };
            co(function* () {
                const repo = yield wizzairRepo;
                yield wizzairService.updatePrice(flight);
                const retrieved = repo.findFlight(flight).then(flight => {
                    expect(flight.prices).to.have.length(1);
                    expect(flight.prices[0].value).to.equal('120');
                    expect(flight.prices[0].currency).to.equal('$');
                });
                expect(retrieved).to.be.fulfilled.notify(done);
            })();

        });

        it('should update price history when flight is updated', (done) => {
            const flight = {
                from: "KBP",
                to: "LDN",
                company: 'WizzAir',
                departureDate: "01/01/2001",
                departureTime: "10:30",
                arrivalTime: "11:30",
                minPrice: {
                    currency: "$",
                    value: "220"
                }
            };
            co(function* () {
                const repo = yield wizzairRepo;
                yield wizzairService.updatePrice(flight);
                const retrieved = repo.findFlight(flight).then(flight => {
                    expect(flight.prices).to.have.length(2);
                    expect(flight.prices[0].value).to.equal('120');
                    expect(flight.prices[0].currency).to.equal('$');
                    expect(flight.prices[1].value).to.equal('220');
                    expect(flight.prices[1].currency).to.equal('$');
                });
                expect(retrieved).to.be.fulfilled.notify(done);
            })();

        });
    });
});
